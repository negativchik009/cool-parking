﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Records;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : Controller
    {
        private readonly IParkingService _parking;
        
        public VehiclesController(IParkingService parking)
        {
            _parking = parking;
        }
        
        // GET api/vehicles
        [HttpGet]
        public ActionResult<IEnumerable<VehicleRecord>> Get()
        {
            return new JsonResult(_parking.GetVehicles()
                .Select(x => new VehicleRecord(x)).ToList());
        }
        
        // GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpGet]
        [Route("{id}")]
        public ActionResult<VehicleRecord> VehicleById(string id)
        {
            try
            {
                if (!Vehicle.IsValidId(id))
                {
                    return StatusCode(400, $"Invalid Id: {id}");
                }
                var vehicle = _parking.GetVehicles().First(x => x.Id == id);
                return new JsonResult(new VehicleRecord(vehicle));
            }
            catch (InvalidOperationException)
            {
                return StatusCode(404, $"There is no vehicle with this Id: {id}");
            }
        }
        
        // POST api/vehicles
        [HttpPost]
        public ActionResult<VehicleRecord> Post([FromBody] VehicleRecord vehicleRecord)
        {
            try
            {
                _parking.AddVehicle(vehicleRecord.GetVehicle());
            }
            catch (ArgumentException)
            {
                return StatusCode(400, $"Invalid body:\n {JsonConvert.SerializeObject(vehicleRecord)}");
            }

            return StatusCode(201, JsonConvert.SerializeObject(vehicleRecord));
        }
        
        // DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpDelete]
        [Route("{id}")]
        public ActionResult DeleteById(string id)
        {
            try
            {
                if (!Vehicle.IsValidId(id))
                {
                    return StatusCode(400, "Invalid Id");
                }
                _parking.RemoveVehicle(id);
                return StatusCode(204);
            }
            catch (ArgumentException)
            {
                return StatusCode(404, $"There is no vehicle with this Id: {id}");
            }
            catch (InvalidOperationException)
            {
                return StatusCode(400, $"Vehicle with Id: {id} have negative balance");
            }
        }
    }
}