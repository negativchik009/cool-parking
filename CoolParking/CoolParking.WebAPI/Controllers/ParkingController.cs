﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : Controller
    {
        private readonly IParkingService _parking;

        public ParkingController(IParkingService parking)
        {
            _parking = parking;
        }

        // GET api/parking/balance
        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> Balance() => _parking.GetBalance();

        // GET api/parking/capacity
        [HttpGet]
        [Route("capacity")]
        public ActionResult<int> Capacity() => _parking.GetCapacity();

        // GET api/parking/freePlaces
        [HttpGet]
        [Route("freePlaces")]
        public ActionResult<int> FreePlaces() => _parking.GetFreePlaces();
    }
}