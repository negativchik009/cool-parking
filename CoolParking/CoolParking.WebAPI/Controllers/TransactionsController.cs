﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Records;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : Controller
    {
        private readonly IParkingService _parking;

        public TransactionsController(IParkingService parking)
        {
            _parking = parking;
        }
        
        // GET api/transactions/last
        [HttpGet]
        [Route("last")]
        public ActionResult<IEnumerable<TransactionRecord>> LastTransactions()
        {
            return _parking.GetLastParkingTransactions().Select(x => new TransactionRecord(x)).ToList();
        }
        
        // GET api/transactions/all (только транзакции с лог файла)
        [HttpGet]
        [Route("all")]
        public ActionResult<string> TransactionsLog()
        {
            return _parking.ReadFromLog();
        }
        
        // PUT api/transactions/topUpVehicle
        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult<VehicleRecord> TopUpVehicle([FromBody] TopUpRecord body)
        {
            try
            {
                if (!Vehicle.IsValidId(body.Id) || body.Sum <= 0 )
                {
                    return StatusCode(400, $"Invalid body: {body}");
                }

                _parking.TopUpVehicle(body.Id, body.Sum);
                return new VehicleRecord(_parking.GetVehicles().First(x => x.Id == body.Id));
            }
            catch (ArgumentException)
            {
                return StatusCode(404, $"There is no vehicle with this Id: {body.Id}");
            }
        }
    }
}