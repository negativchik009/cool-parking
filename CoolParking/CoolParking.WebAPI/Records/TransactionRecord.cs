﻿using System;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Records
{
    public record TransactionRecord
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; init; }
        [JsonProperty("sum")]
        public decimal Sum { get; init;  }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDateTime { get; init; }
        public TransactionRecord() {}

        public TransactionRecord(TransactionInfo transactionInfo)
        {
            VehicleId = transactionInfo.VehicleId;
            Sum = transactionInfo.Sum;
            TransactionDateTime = transactionInfo.TransactionDateTime;
        }
    }
}