﻿using Newtonsoft.Json;

namespace CoolParking.WebAPI.Records
{
    public record TopUpRecord
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
        
        public TopUpRecord() {}
    }
}