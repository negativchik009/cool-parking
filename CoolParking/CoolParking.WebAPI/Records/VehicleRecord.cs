﻿using System;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Records
{
    public record VehicleRecord
    {
        [JsonProperty("id")] 
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        // Passenger - 1, Truck - 2, Bus - 3, Motorcycle - 4
        public int VehicleType { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
        
        public Vehicle GetVehicle()
        {
            var type = VehicleType switch
                {
                    1 => BL.Models.VehicleType.PassengerCar,
                    2 => BL.Models.VehicleType.Truck,
                    3 => BL.Models.VehicleType.Bus,
                    4 => BL.Models.VehicleType.Motorcycle,
                    _ => throw new ArgumentException("Wrong number of type")
                };
            return new Vehicle(Id, type, Balance);
        }
        
        public VehicleRecord() {}

        public VehicleRecord(Vehicle vehicle)
        {
            Id = vehicle.Id;
            VehicleType = (int) vehicle.VehicleType;
            Balance = vehicle.Balance;
        }
    }
}