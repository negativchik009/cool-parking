﻿using System;
using System.Net.Http;
using System.Text;
using CoolParking.Client.Controllers;
using CoolParking.Client.Printers;

namespace CoolParking.Client
{
    public static class Program
    {
        public static void Main(params string[] args)
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            var client = new HttpClient
            {
                BaseAddress = new Uri("https://localhost:5001")
            };
            var wrapper = new HttpWrapper.HttpWrapper(client);
            var printer = new ConsolePrinter();
            var parking = new ParkingService(wrapper);
            var controller = new Controller(parking, printer);
            controller.Work();
        }
    }
}