﻿using System.Collections.Generic;
using System.Linq;
using CoolParking.Client.HttpWrapper;
using CoolParking.Client.Records;

namespace CoolParking.Client
{
    public class ParkingService
    {
        private readonly IHttpWrapper _wrapper;

        public ParkingService(IHttpWrapper wrapper)
        {
            _wrapper = wrapper;
            Balance =  _wrapper.CurrentBalance().Result;
            PeriodBalance =  _wrapper.PeriodBalance().Result;
            Capacity =  _wrapper.Capacity().Result;
            FreePlaces =  _wrapper.FreePlaces().Result;
            Vehicles = _wrapper.Vehicles().Result.ToList();
            LastTransactions =  _wrapper.LastTransactions().Result;
            Log =  _wrapper.TransactionsLog().Result;
        }

        public decimal Balance { get; private set; }
        
        public decimal PeriodBalance { get; private set; }

        public int Capacity { get; private set; }

        public int FreePlaces { get; private set; }

        public List<VehicleRecord> Vehicles { get; private set; }
        
        public TransactionRecord[] LastTransactions { get; private set; }
        
        public string Log { get; private set; }

        public bool AddVehicle(VehicleRecord vehicle)
        {
            if (!_wrapper.AddVehicle(vehicle).Result) return false;
            Refresh();
            return true;
        }

        public bool RemoveVehicle(string vehicleId)
        {
            if (!_wrapper.RemoveVehicle(vehicleId).Result) return false;
            Refresh();
            return true;
        }

        public bool TopUpVehicle(string vehicleId, decimal sum)
        {
            if (!_wrapper.TopUpVehicle(vehicleId, sum).Result) return false;
            Refresh();
            return true;
        }

        public async void Refresh()
        {
            Balance = await _wrapper.CurrentBalance();
            PeriodBalance = await _wrapper.PeriodBalance();
            Capacity = await _wrapper.Capacity();
            FreePlaces = await _wrapper.FreePlaces();
            Vehicles = (await _wrapper.Vehicles()).ToList();
            LastTransactions = await _wrapper.LastTransactions();
            Log = await _wrapper.TransactionsLog();
        }
    }
}