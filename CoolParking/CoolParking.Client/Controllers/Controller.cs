﻿using System;
using System.Linq;
using System.Net.Http;
using CoolParking.Client.Enums;
using CoolParking.Client.Printers;
using CoolParking.Client.Records;

namespace CoolParking.Client.Controllers
{
    public class Controller
    {
        private readonly ParkingService _parking;
        private readonly IPrinter _printer;
        private string message = "";
        private ApplicationState state;
        
        public Controller(ParkingService parking, IPrinter printer)
        {
            _parking = parking;
            _printer = printer;
        }

        public void Work()
        {
            while (true)
            {
                _printer.Reset();
                _printer.PrintMessage(message);
                message = "";
                _printer.PrintHeader();
                _printer.PrintInformation(
                    _parking.Balance,
                    _parking.PeriodBalance,
                    _parking.FreePlaces,
                    _parking.Capacity
                    );
                switch (state)
                {
                    case ApplicationState.LastTransactions:
                        _printer.PrintMainScreen(string.Join('\n',
                            _parking.LastTransactions
                                .Select(x=>x.ToString())));
                        break;
                    case ApplicationState.TransactionsLog:
                        _printer.PrintMainScreen(_parking.Log);
                        break;
                    case ApplicationState.Vehicles:
                        _printer.PrintMainScreen(string.Join('\n',
                            _parking.Vehicles
                                .Select(x=>x.ToString())));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                VehicleRecord vehicleRecord;
                switch (_printer.GetCommand())
                {
                    case Command.ShowLastTransactions:
                        state = ApplicationState.LastTransactions;
                        message = "Последние транзакции";
                        break;
                    case Command.ShowTransactionsLog:
                        state = ApplicationState.TransactionsLog;
                        message = "Лог транзакций";
                        break;
                    case Command.ShowVehicles:
                        state = ApplicationState.Vehicles;
                        message = "ТС на парковке";
                        break;
                    case Command.AddVehicle:
                        vehicleRecord = _printer.GetData(TakeDataType.AddVehicle);
                        if (_parking.AddVehicle(new VehicleRecord(vehicleRecord.Id, vehicleRecord.Type,
                            vehicleRecord.Balance)))
                        {
                            message = "Операция успешна";
                        }
                        message = "Возникла ошибка";
                        break;
                    case Command.RemoveVehicle:
                        vehicleRecord = _printer.GetData(TakeDataType.RemoveVehicle);
                        if (_parking.RemoveVehicle(vehicleRecord.Id))
                        {
                            message = "Операция успешна";
                        }
                        message = "Возникла ошибка";
                        break;
                    case Command.TopUpVehicle:
                        vehicleRecord = _printer.GetData(TakeDataType.TopUpVehicle);
                        if (_parking.TopUpVehicle(vehicleRecord.Id, vehicleRecord.Balance))
                        {
                            message = "Операция успешна";
                        }
                        message = "Возникла ошибка";
                        break;
                    case Command.WrongInput:
                        message = "Невозможно обработать команду";
                        break;
                    case Command.Refresh:
                        _parking.Refresh();
                        message = "Данные успешно обновлены";
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}