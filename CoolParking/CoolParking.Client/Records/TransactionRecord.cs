﻿using System;
using Newtonsoft.Json;

namespace CoolParking.Client.Records
{
    public record TransactionRecord
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; init; }
        [JsonProperty("sum")]
        public decimal Sum { get; init;  }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDateTime { get; init; }
        public TransactionRecord() {}
        public override string ToString()
        {
            // AE-1234-EA: 5, 31/04/21. 12:09:31
            return
                $"{VehicleId}: {Sum}, {TransactionDateTime.Day}/{TransactionDateTime.Month}/{TransactionDateTime.Year}. " +
                $"{TransactionDateTime.Hour}:{TransactionDateTime.Minute}:{TransactionDateTime.Second}";
        }
    }
}