﻿using CoolParking.Client.Enums;
using Newtonsoft.Json;

namespace CoolParking.Client.Records
{
    [JsonObject(MemberSerialization.OptIn)]
    public record VehicleRecord
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public VehicleType Type { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public VehicleRecord() {}
        
        public VehicleRecord(string id, VehicleType type, decimal balance)
        {
            Id = id;
            Type = type;
            Balance = balance;
        }
    }
}