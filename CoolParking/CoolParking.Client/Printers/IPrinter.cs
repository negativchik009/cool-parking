﻿using CoolParking.Client.Enums;
using CoolParking.Client.Records;

namespace CoolParking.Client.Printers
{
    public interface IPrinter
    {
        public void PrintMessage(string message);
        public void PrintHeader();
        public void PrintInformation(decimal balance, decimal sessionBalance, int freePlaces, int capacity);
        public void PrintMainScreen(string data);
        public VehicleRecord GetData(TakeDataType type);
        public Command GetCommand();
        public void Reset();
    }
}