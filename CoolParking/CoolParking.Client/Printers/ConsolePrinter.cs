﻿using System;
using System.Text.RegularExpressions;
using CoolParking.Client.Enums;
using CoolParking.Client.Records;

namespace CoolParking.Client.Printers
{
    public class ConsolePrinter : IPrinter
    {
        private const string Pattern = "[A-Z]{2}-[0-9]{4}-[A-Z]{2}";
        public void PrintMessage(string message)
        {
            Console.WriteLine("Для обновления информации нажмите 7");
            PrintSeparator();
            Console.WriteLine(message);
            PrintSeparator();
        }

        public void PrintHeader()
        {
            Console.WriteLine(
                $"{"1 - Вывести все транзакции за период",-40}║ {"2 - Вывести историю транзакций",-40}║ {"3 - Вывести список транспортных средств",-40}");
            PrintSeparator();
            Console.WriteLine(
                $"{"4 - Добавить ТС на парковку",-40}║ {"5 - Убрать ТС с парковки",-40}║ {"6 - Пополнить баланс ТС",-40}");
            PrintSeparator();
        }

        public void PrintInformation(decimal balance, decimal sessionBalance, int freePlaces, int capacity)
        {
            Console.WriteLine(
                $"Баланс: {balance,-32}║ Баланс за сессию: {sessionBalance,-22}║ Свободно/всего мест: {freePlaces}/{capacity}");
            PrintSeparator();
        }

        public void PrintMainScreen(string data)
        {
            Console.WriteLine(data);
        }

        public VehicleRecord GetData(TakeDataType type)
        {
            string buffer;
            var result = new VehicleRecord();
            while (true)
            {
                Console.WriteLine("Введите номер ТС");
                buffer = Console.ReadLine();
                if (Regex.IsMatch(buffer??"", Pattern))
                {
                    result.Id = buffer;
                    break;
                }
                Console.WriteLine("Неверный формат ввода. Пример правильного ввода: ZZ-0000-ZZ (где вместо Z - любая латинкая буква, а вместо 0 - любая цифра");
            }
            switch (type)
            {
                case TakeDataType.AddVehicle:
                {
                    while (true)
                    {
                        Console.WriteLine("Введите тип ТС (1 - Легковой, 2 - Грузовой, 3 - Автобус, 4 - Мотоцикл");
                        buffer = Console.ReadLine();
                        if (buffer == "1" || buffer == "2" || buffer == "3" || buffer == "4")
                        {
                            result.Type = buffer switch
                            {
                                "1" => VehicleType.PassengerCar,
                                "2" => VehicleType.Truck,
                                "3" => VehicleType.Bus,
                                "4" => VehicleType.Motorcycle,
                            };
                            break;
                        }
                        Console.WriteLine("Неверный формат ввода. Введите число от 1 до 4 включительно");
                    }
                    while (true)
                    {
                        Console.WriteLine("Введите начальный баланс ТС");
                        buffer = Console.ReadLine();
                        decimal buff;
                        if (decimal.TryParse(buffer, out buff))
                        {
                            result.Balance = buff;
                            break;
                        }
                        Console.WriteLine("Неверный формат ввода. Введите вещественное число больше нуля");
                    }
                    return result;
                }
                case TakeDataType.RemoveVehicle:
                    return result;
                case TakeDataType.TopUpVehicle:
                    while (true)
                    {
                        Console.WriteLine("Введите сумму пополнения баланса ТС");
                        buffer = Console.ReadLine();
                        decimal buff;
                        if (decimal.TryParse(buffer, out buff))
                        {
                            result.Balance = buff;
                            break;
                        }
                        Console.WriteLine("Неверный формат ввода. Введите вещественное число больше нуля");
                    }
                    return result;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public Command GetCommand()
        {
            return Console.ReadKey(true).Key switch
                {
                    ConsoleKey.D1 => Command.ShowLastTransactions,
                    ConsoleKey.D2 => Command.ShowTransactionsLog,
                    ConsoleKey.D3 => Command.ShowVehicles,
                    ConsoleKey.D4 => Command.AddVehicle,
                    ConsoleKey.D5 => Command.RemoveVehicle,
                    ConsoleKey.D6 => Command.TopUpVehicle,
                    ConsoleKey.D7 => Command.Refresh,
                    _ => Command.WrongInput
                };
        }

        public void Reset()
        {
            Console.Clear();
        }
        
        private void PrintSeparator()
        {
            for (int i = 0; i < 125; i++)
            {
                if (i == 40 || i == 82)
                {
                    Console.Write("╬");
                    continue;
                }
                Console.Write("=");
            }
            Console.WriteLine();
        }
    }
}