﻿using System.Threading.Tasks;
using CoolParking.Client.Records;

namespace CoolParking.Client.HttpWrapper
{
    public interface IHttpWrapper
    {
        public Task<decimal> CurrentBalance();
        public Task<decimal> PeriodBalance();
        public Task<int> Capacity();
        public Task<int> FreePlaces();
        public Task<TransactionRecord[]> LastTransactions();
        public Task<string> TransactionsLog();
        public Task<VehicleRecord[]> Vehicles();
        public Task<bool> AddVehicle(VehicleRecord vehicle);
        public Task<bool> RemoveVehicle(string vehicleId);
        public Task<bool> TopUpVehicle(string vehicleId, decimal sum);
    }
}