﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.Client.Records;
using Newtonsoft.Json;

namespace CoolParking.Client.HttpWrapper
{
    public class HttpWrapper : IHttpWrapper
    {
        private readonly HttpClient _client;

        public HttpWrapper(HttpClient client)
        {
            _client = client;
        }
        
        public async Task<decimal> CurrentBalance()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/parking/balance");
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Not success request");
            }
            return JsonConvert.DeserializeObject<decimal>(new StreamReader(response.Content.ReadAsStream()).ReadToEnd());
        }

        public async Task<decimal> PeriodBalance()
        {
            return (await LastTransactions()).Sum(x => x.Sum);
        }

        public async Task<int> Capacity()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/parking/capacity");
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Not success request");
            }
            return JsonConvert.DeserializeObject<int>(new StreamReader(response.Content.ReadAsStream()).ReadToEnd());
        }

        public async Task<int> FreePlaces()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/parking/freePlaces");
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Not success request");
            }
            return JsonConvert.DeserializeObject<int>(new StreamReader(response.Content.ReadAsStream()).ReadToEnd());
        }

        public async Task<TransactionRecord[]> LastTransactions()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/transactions/last");
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Not success request");
            }
            return JsonConvert.DeserializeObject<TransactionRecord[]>(new StreamReader(response.Content.ReadAsStream()).ReadToEnd());
        }

        public async Task<string> TransactionsLog()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/transactions/all");
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Not success request");
            }
            return new StreamReader(response.Content.ReadAsStream()).ReadToEnd();
        }

        public async Task<VehicleRecord[]> Vehicles()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/vehicles");
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Not success request");
            }
            return JsonConvert.DeserializeObject<VehicleRecord[]>(new StreamReader(response.Content.ReadAsStream()).ReadToEnd());
        }

        public async Task<bool> AddVehicle(VehicleRecord vehicle)
        {
            var response = await _client.PostAsync("/api/vehicles", 
                new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.UTF8, "application/json"));
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> RemoveVehicle(string vehicleId)
        {
            var response = await _client.DeleteAsync($"/api/vehicles/{vehicleId}");
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> TopUpVehicle(string vehicleId, decimal sum)
        {
            var response = await _client.PutAsync("/api/transactions/topUpVehicle", 
                new StringContent(JsonConvert.SerializeObject(new TopUpRecord(){Id = vehicleId, Sum = sum}), 
                    Encoding.UTF8, "application/json"));
            return response.IsSuccessStatusCode;
        }
    }
}