﻿namespace CoolParking.Client.Enums
{
    /// <summary>
    /// Enum describing a type of vehicle
    /// </summary>
    public enum VehicleType
    {
        PassengerCar = 1,
        Truck = 2,
        Bus = 3,
        Motorcycle = 4
    }
}