﻿namespace CoolParking.Client.Enums
{
    public enum TakeDataType
    {
        AddVehicle,
        RemoveVehicle,
        TopUpVehicle
    }
}