﻿namespace CoolParking.Client.Enums
{
    public enum ApplicationState
    {
        LastTransactions,
        Vehicles,
        TransactionsLog
    }
}