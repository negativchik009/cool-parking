﻿namespace CoolParking.Client.Enums
{
    public enum Command
    {
        ShowLastTransactions,
        ShowTransactionsLog,
        ShowVehicles,
        AddVehicle,
        RemoveVehicle,
        TopUpVehicle,
        WrongInput,
        Refresh
    }
}