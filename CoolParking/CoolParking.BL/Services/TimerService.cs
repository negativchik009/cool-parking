﻿using System;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer _timer;
        public event ElapsedEventHandler Elapsed = delegate(object sender, ElapsedEventArgs args) {  };
        
        public double Interval
        {
            get => _timer.Interval;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Interval can`t be negative");
                }
                if (_timer.Enabled)
                {
                    _timer.Stop();
                }

                _timer.Interval = value;
            }
        }

        public TimerService(double interval)
        {
            if (interval <= 0)
            {
                throw new ArgumentException("Interval can`t be negative"); 
            }
            _timer = new Timer(interval) { AutoReset = true };
            Interval = interval;
            _timer.Elapsed += OnTick;
        }

        private void OnTick(object o, ElapsedEventArgs e)
        {
            Elapsed.Invoke(this, e);
        }
        
        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}