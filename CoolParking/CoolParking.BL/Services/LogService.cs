﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public void Write(string logInfo)
        {
            using (var writer = new StreamWriter(LogPath, true))
            {
                writer.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("File not found");
            }
            using (var reader = new StreamReader(LogPath))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
