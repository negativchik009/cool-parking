﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    /// <summary>
    /// Parking Service class. Wrapper around Parking, automatize work
    /// of Parking with ITimerService implementation, log with ILogService implementation   
    /// </summary>
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly List<TransactionInfo> _transactions;
        private decimal _balance;

        public bool IsHaveFreePlaces => _parking.Vehicles.Count < _parking.Capacity;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _parking = Parking.Instance;
            _balance = 0;
            _transactions = new List<TransactionInfo>();
            _withdrawTimer.Elapsed += Withdraw;
            _logTimer.Elapsed += WriteToLog;
            _withdrawTimer.Start();
            _logTimer.Start();
        }
        
        public void Dispose()
        {
            _parking.Vehicles.Clear();
            _parking.Balance = 0;
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public decimal GetPeriodBalance()
        {
            return _balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (!IsHaveFreePlaces)
            {
                throw new InvalidOperationException("No free places on Parking");
            }
            if (_parking.Vehicles.Any(x => x.Id == vehicle.Id))
            {
                throw new ArgumentException("Parking already contains vehicle with this Id");
            }
            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles
                .FirstOrDefault(x=> x.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("There is no vehicle with this Id");
            }

            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Can`t remove debtor");
            }
            _parking.Vehicles.Remove(vehicle);
        }
        
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.Vehicles
                .FirstOrDefault(x => x.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("There is no vehicle with this Id");
            }

            if (sum <= 0)
            {
                throw new ArgumentException("Invalid top up sum");
            }

            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }
        
        private void WriteToLog(object o, ElapsedEventArgs e)
        {
            _logService.Write(string.Join("\n", _transactions.Select(x=>x.ToString())));
            _transactions.Clear();
            _balance = 0;
        }

        /// <summary>
        /// Automatic withdraw. For each Vehicle calculate sum of withdraw based on them balance
        /// </summary>
        private void Withdraw(object o, ElapsedEventArgs e)
        {
            decimal diff;
            foreach (var vehicle in _parking.Vehicles)
            {
                if (vehicle.Balance < 0)
                {
                    diff = Settings.Withdraw[vehicle.VehicleType] * Settings.PenaltyMultiplier;
                }
                else if (vehicle.Balance < Settings.Withdraw[vehicle.VehicleType])
                {
                    diff = vehicle.Balance +
                           (Settings.Withdraw[vehicle.VehicleType] - vehicle.Balance) *
                           Settings.PenaltyMultiplier;
                }
                else
                {
                    diff = Settings.Withdraw[vehicle.VehicleType];
                }

                _balance += diff;
                _parking.Balance += diff;
                vehicle.Balance -= diff;
                _transactions.Add(new TransactionInfo
                {
                    VehicleId = vehicle.Id,
                    Sum = diff,
                    TransactionDateTime = DateTime.Now
                });
            }
        }
    }
}