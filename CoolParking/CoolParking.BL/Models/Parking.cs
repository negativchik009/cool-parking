﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    internal class Parking
    {
        private static readonly Lazy<Parking> _instance = new Lazy<Parking>(() => new Parking());
        
        public decimal Balance { get; internal set; }
        public List<Vehicle> Vehicles { get; internal set; }
        public int Capacity { get; }

        private Parking()
        {
            Balance = Settings.StartBalance;
            Capacity = Settings.ParkingCapacity;
            Vehicles = new List<Vehicle>();
        }

        public static Parking Instance => _instance.Value;
    }
}