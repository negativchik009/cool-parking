﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance { get; }
        public static int ParkingCapacity { get; }
        public static int LogPeriod { get; }
        public static int WithdrawPeriod { get; }
        public static Dictionary<VehicleType, decimal> Withdraw { get; }
        public static decimal PenaltyMultiplier { get; }

        static Settings()
        {
            StartBalance = 0;
            ParkingCapacity = 10;
            WithdrawPeriod = 5000;
            LogPeriod = 60000;
            Withdraw = new Dictionary<VehicleType, decimal>
            {
                {VehicleType.PassengerCar, 2},
                {VehicleType.Truck, 5},
                {VehicleType.Bus, new decimal(3.5)},
                {VehicleType.Motorcycle, 1}
            };
            PenaltyMultiplier = new decimal(2.5);
        }
    }
}