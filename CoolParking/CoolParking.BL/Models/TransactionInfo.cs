﻿using System;

namespace CoolParking.BL.Models
{
    /// <summary>
    /// Record, that represent a Transaction 
    /// </summary>
    public record TransactionInfo
    {
        public decimal Sum { get; init;  }
        public string VehicleId { get; init; }
        public DateTime TransactionDateTime { get; init; }

        public override string ToString()
        {
            // AE-1234-EA: 5, 31/04/21. 12:09:31
            return
                $"{VehicleId}: {Sum}, {TransactionDateTime.Day}/{TransactionDateTime.Month}/{TransactionDateTime.Year}. " +
                $"{TransactionDateTime.Hour}:{TransactionDateTime.Minute}:{TransactionDateTime.Second}";
        }
    }
}