﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }

        public decimal Balance { get; internal set; }
        private const string Pattern = "[A-Z]{2}-[0-9]{4}-[A-Z]{2}";

        public Vehicle(string id, VehicleType type, decimal balance = 0)
        {
            if (!IsValidId(id))
            {
                throw new ArgumentException("Id doesn`t match the pattern");
            }

            if (balance < 0)
            {
                throw new ArgumentException("Start balance can`t be negative");
            }

            Id = id;
            VehicleType = type;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rnd = new Random();
            // Generating array with random numbers
            // 2 first and 2 last for letters, else - numbers
            int[] numbers = new[]
            {
                rnd.Next(65, 91),
                rnd.Next(65, 91),
                rnd.Next(0, 10),
                rnd.Next(0, 10),
                rnd.Next(0, 10),
                rnd.Next(0, 10),
                rnd.Next(65, 91),
                rnd.Next(65, 91)
            };
            return
                $"{(char)numbers[0]}{(char)numbers[1]}-{numbers[2]}{numbers[3]}{numbers[4]}{numbers[5]}-{(char)numbers[6]}{(char)numbers[7]}";
        }

        public override string ToString()
        {
            return $"{Id} Баланс: {Balance}. Тип ТС: {VehicleType}";
        }

        public static bool IsValidId(string id)
        {
            return Regex.IsMatch(id.ToUpper(), Pattern);
        }
    }
}